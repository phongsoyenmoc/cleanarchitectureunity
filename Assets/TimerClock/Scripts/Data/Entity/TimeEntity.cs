using CAFU.Core.Data.Entity;

namespace Timer.Data.Entity
{
	public interface ITimeEntity : IEntity
	{
		float CurrentTime { get; set; }
		float MaxTime { get; set; }
	}

	public class TimeEntity : ITimeEntity
	{
		private float time = 0;
		private float maxTime = 0;

		public float CurrentTime { get => time; set => time = value; }
		public float MaxTime { get => maxTime; set => maxTime = value; }
	}
}