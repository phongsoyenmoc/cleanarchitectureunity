#pragma warning disable 0649
using CAFU.Core.Presentation.View;
using System;
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace Timer.Presentation.View.Main
{
	public class TimerView : MonoBehaviour, IView
	{
		[SerializeField] Text timerText;
		private void Start ()
		{
			Run ();
		}

		private void Run ()
		{
			timerText.gameObject.SetActive (true);
			IDisposable dispose = Observable.EveryUpdate ()
				.Subscribe (_ =>
				{
					this.GetPresenter ().AddTime (Time.deltaTime);
					timerText.text = this.GetPresenter ().TimerDisplayText;
				}).AddTo(gameObject);
			this.GetPresenter ().EndTimeSubject.Subscribe (_ =>
			{
				dispose.Dispose ();
			}).AddTo (this.gameObject);
		}
	}
}