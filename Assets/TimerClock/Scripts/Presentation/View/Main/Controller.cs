using CAFU.Core.Presentation.View;
using Timer.Presentation.Presenter;

namespace Timer.Presentation.View.Main
{
    public class Controller : Controller<Controller, MainPresenter, MainPresenter.Factory>
    {
        protected override void OnStart()
        {
            base.OnStart();
        }
    }

    public static class ViewExtension
    {
        public static MainPresenter GetPresenter(this IView view)
        {
            return Controller.Instance.Presenter;
        }
    }
}