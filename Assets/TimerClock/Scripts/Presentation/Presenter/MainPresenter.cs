using CAFU.Core.Presentation.Presenter;
using Domain.UseCase;

namespace Timer.Presentation.Presenter
{
	public class MainPresenter : IPresenter
	{
		private ITimerUseCase timerUseCase { get; set; }
		public void AddTime (float addTime)
		{
			timerUseCase.AddTime (addTime);
		}
		public string TimerDisplayText
		{
			get { return timerUseCase.CurrentTimeDisplayText; }
		}
		public UniRx.Subject<int> EndTimeSubject
		{
			get { return timerUseCase.EndTimeSubject; }
		}
		public class Factory : DefaultPresenterFactory<MainPresenter>
		{
			protected override void Initialize (MainPresenter instance)
			{
				base.Initialize (instance);
				instance.timerUseCase = new TimerUseCase.Factory ().Create ();
			}
		}
	}
}