using CAFU.Core.Domain.Translator;
using Timer.Data.Entity;
using Timer.Domain.Model;

namespace Domain.Translator
{
	public class TimeModelTranslator : IModelTranslator<ITimeEntity, ITimeModel>
	{
		public ITimeModel Translate (ITimeEntity entity)
		{
			return new TimeModel (entity.CurrentTime, entity.MaxTime);
		}
	}
}