using CAFU.Core.Domain.Model;

namespace Timer.Domain.Model
{
	public interface ITimeModel : IModel
	{
		float MaxTime { get; }
		float CurrentTime { get; }
		float RemainTime { get; }
		void AddCurrentTime (float addTime);
	}

	public class TimeModel : ITimeModel
	{
		public float MaxTime { get; }

		public float CurrentTime { get; set; }

		public float RemainTime { get => MaxTime - CurrentTime; }

		public TimeModel (float currentTime, float maxTime)
		{
			CurrentTime = currentTime;
			MaxTime = maxTime;
		}

		public void AddCurrentTime (float addTime)
		{
			CurrentTime += addTime;
		}
	}
}