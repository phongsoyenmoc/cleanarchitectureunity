using CAFU.Core.Domain.Translator;
using CAFU.Core.Domain.UseCase;
using CAFU.Generics.Domain.Repository;
using Domain.Translator;
using Timer.Data.Entity;
using Timer.Domain.Model;
using UniRx;

namespace Domain.UseCase
{
	public interface ITimerUseCase : IUseCase
	{
		void AddTime (float addTime);
		Subject<int> EndTimeSubject { get; }
		string CurrentTimeDisplayText { get; }

	}

	public class TimerUseCase : ITimerUseCase
	{
		private ITimeModel timeModel;
		private Subject<int> endTimeSubject = new Subject<int> ();
		public string CurrentTimeDisplayText
		{
			get {
				int min = (int) (timeModel.RemainTime / 60);
				int second = (int) (timeModel.RemainTime % 60);
				return min.ToString ("00") + ":" + second.ToString ("00");
			}
		}

		public Subject<int> EndTimeSubject => endTimeSubject;

		public void AddTime (float addTime)
		{
			timeModel.AddCurrentTime (addTime);
			if (timeModel.RemainTime <= 0)
			{
				EndTimeSubject.OnNext (0);
				EndTimeSubject.OnCompleted ();
			}
		}

		public class Factory : DefaultUseCaseFactory<TimerUseCase>
		{
			protected override void Initialize (TimerUseCase instance)
			{
				base.Initialize (instance);
				TimeEntity entity = new TimeEntity();
				entity.CurrentTime = 0;
				entity.MaxTime = 10;
				instance.timeModel = new DefaultTranslatorFactory<TimeModelTranslator> ().Create ().Translate (entity);
			}
		}
	}
}